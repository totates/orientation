import logging from "external:@totates/logging/v1";
import preferences from "external:@totates/preferences/v1";
import EventEmitter from "external:@totates/events/v1";
import debounce from "lodash.debounce"; // TODO: lodash.{debounce,throttle} component

const logger = logging.getLogger("@totates/orientation/v1");
logger.setLevel("DEBUG");

const ORIENTATION = "orientation";

class Orientation extends EventEmitter {
    constructor() {
        super();
        this.initialized = this.init();
        this.saveOrientation = debounce(() => this._save().catch(logger.error), 1000);
    }

    async init() {
        // this.orientation = await this._load();
        // logger.debug(ORIENTATION, this.orientation);

        try {
            var sensor = new window.AbsoluteOrientationSensor();
        }
        catch (e) {
            logger.debug("not available:", e.message);
            this.available = false;
            return;
        }

        try {
            await new Promise((resolve, reject) => {
                sensor.onactivate = e => {
                    sensor.stop();
                    resolve();
                };
                sensor.onerror = e => {
                    sensor.stop();
                    reject(e);
                };
                sensor.start();
            });
        }
        catch (e) {
            logger.debug("not available:", e.error.message);
            this.available = false;
            return;
        }
        logger.debug("orientation is available");
        this.available = true;
    }

    async _load() {
        this.orientation = await preferences.get(
            ORIENTATION, ORIENTATION, { alpha: 0, beta: 0, gamma: 0 }
        );
    }

    async _save() {
        await preferences.set(ORIENTATION, ORIENTATION, this.orientation);
    }

    setOrientation(orientation) {
        this.orientation = orientation;
        this._save();
        this.emit(ORIENTATION, orientation);
    }

    isAvailable() {
        return this.available;
    }

    readingEnabled() {
        return !!this.sensor;
    }

    disableReading() {
        if (this.sensor) {
            this.sensor.stop();
            this.sensor = null;
            logger.debug("readings disabled");
        }
    }

    async enableReading(frequency = 10/* Hz*/) {
        if (!window.AbsoluteOrientationSensor) {
            logger.warn("no sensor");
            return;
        }

        if (this.sensor) /** @todo return silently if frequency is the same? */
            throw new Error("Readings already enabled");

        // if (event.error.name === "NotAllowedError") {
        //     // Branch to code for requesting permission.
        // }
        // else if (event.error.name === "NotReadableError" )
        //     console.log("Cannot connect to the sensor.");

        /** @todo only query permission if NotAllowedError on start? */
        const names = ["accelerometer", "magnetometer", "gyroscope"];
        const requests = names.map(name => navigator.permissions.query({ name }));
        const results = await Promise.allSettled(requests);
        if (!results.every(({ status, value }) => status === "fulfilled" && value.state === "granted"))
            logger.warn("some persmissions were not granted:", results);

        await new Promise((resolve, reject) => {

            const sensor = new window.AbsoluteOrientationSensor({ frequency });

            /* sensor structure in chromium 73
             * see https://github.com/w3c/orientation-sensor/issues/43
             activated: true
             hasReading: true
             onactivate: null
             onerror: null
             onreading: null
             quaternion: (4) [
                 0.018470941111445427,
                 0.018363816663622856,
                 0.9749502539634705,
                 0.2208927720785141
             ]
             timestamp: 1546766313494.2
            */
            const trigger = () => {
                const [w, x, y, z] = sensor.quaternion;
                const euler = {
                    alpha: Math.atan2(2 * (w * x + y * z), 1 - (2 * (x * x + y * y))),
                    beta: Math.asin(2 * (w * y - z * x)),
                    gamma: Math.atan2(2 * (w * z + x * y), 1 - (2 * (y * y + z * z)))
                };
                this.setOrientation({
                    quaternion: sensor.quaternion, timestamp: sensor.timestamp, euler
                });
            };

            if (sensor.hasReading) /** @todo useful? */
                trigger();

            function onError(e) {
                logger.error("could not start:", e.error.name);
                reject(e);
            }

            sensor.addEventListener("reading", trigger);
            sensor.addEventListener("error", onError);
            sensor.addEventListener("activate", e => {
                logger.debug(`readings enabled at ${frequency}Hz`);
                sensor.removeEventListener("error", onError);
                sensor.addEventListener("error", logger.error);
                this.sensor = sensor;
                resolve(sensor);
            });
            sensor.start();
        });
    }
}

const orientation = new Orientation();
export default orientation;
